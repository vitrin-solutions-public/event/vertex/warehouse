<?php

namespace Vitrin\Event\Warehouse\Shelf;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ShelfUpdated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
        public array $properties = [],
    ) {
        //
    }
}

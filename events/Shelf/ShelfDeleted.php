<?php

namespace Vitrin\Event\Warehouse\Shelf;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ShelfDeleted extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
    ) {
        //
    }
}

<?php

namespace Vitrin\Event\Warehouse\Shelf;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ShelfCreated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}

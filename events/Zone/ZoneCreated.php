<?php

namespace Vitrin\Event\Warehouse\Zone;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ZoneCreated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}

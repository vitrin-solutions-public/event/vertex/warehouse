<?php

namespace Vitrin\Event\Warehouse\Zone;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ZoneUpdated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
        public array $properties = [],
    ) {
        //
    }
}

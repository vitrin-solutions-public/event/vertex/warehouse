<?php

namespace Vitrin\Event\Warehouse\Zone;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class ZoneDeleted extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
    ) {
        //
    }
}

<?php

namespace Vitrin\Event\Warehouse\Warehouse;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class WarehouseCreated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}

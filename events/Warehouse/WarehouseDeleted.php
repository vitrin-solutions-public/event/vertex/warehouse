<?php

namespace Vitrin\Event\Warehouse\Warehouse;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class WarehouseDeleted extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
    ) {
        //
    }
}

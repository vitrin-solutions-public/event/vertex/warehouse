<?php

namespace Vitrin\Event\Warehouse\Box;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class BoxUpdated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
        public array $properties = [],
    ) {
        //
    }
}

<?php

namespace Vitrin\Event\Warehouse\Box;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class BoxDeleted extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
    ) {
        //
    }
}
